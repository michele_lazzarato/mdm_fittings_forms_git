// ---------------------------------------------------------------------------
// This file has been generated partially by the Web Dynpro Code Generator.
// MODIFY CODE ONLY IN SECTIONS ENCLOSED BY @@begin AND @@end.
// ALL OTHER CHANGES WILL BE LOST IF THE FILE IS REGENERATED.
// ---------------------------------------------------------------------------
package com.manuli;

// 
// IMPORTANT NOTE: 
// _ALL_ IMPORT STATEMENTS MUST BE PLACED IN THE FOLLOWING SECTION ENCLOSED
// BY @@begin imports AND @@end. FURTHERMORE, THIS SECTION MUST ALWAYS CONTAIN
// AT LEAST ONE IMPORT STATEMENT (E.G. THAT FOR IPrivateMDM_Fittings_form_CompView).
// OTHERWISE, USING THE ECLIPSE FUNCTION "Organize Imports" FOLLOWED BY
// A WEB DYNPRO CODE GENERATION (E.G. PROJECT BUILD) WILL RESULT IN THE LOSS
// OF IMPORT STATEMENTS.
//
//@@begin imports
import com.manuli.wdp.IPrivateMDM_Fittings_form_CompView;
import com.sap.tc.webdynpro.clientserver.adobe.pdfdocument.api.*;
import com.sap.tc.webdynpro.basesrvc.util.IOUtil;
import com.sap.tc.webdynpro.services.sal.url.api.*;
import com.sap.tc.webdynpro.services.session.api.*;
import java.io.*;

//@@end

//@@begin documentation
//@@end

public class MDM_Fittings_form_CompView
{
  /**
   * Logging location.
   */
  private static final com.sap.tc.logging.Location logger = 
    com.sap.tc.logging.Location.getLocation(MDM_Fittings_form_CompView.class);

  static 
  {
    //@@begin id
    String id = "$Id$";
    //@@end
    com.sap.tc.logging.Location.getLocation("ID.com.sap.tc.webdynpro").infoT(id);
  }

  /**
   * Private access to the generated Web Dynpro counterpart 
   * for this controller class.  </p>
   *
   * Use <code>wdThis</code> to gain typed access to the context,
   * to trigger navigation via outbound plugs, to get and enable/disable
   * actions, fire declared events, and access used controllers and/or 
   * component usages.
   *
   * @see com.manuli.wdp.IPrivateMDM_Fittings_form_CompView for more details
   */
  private final IPrivateMDM_Fittings_form_CompView wdThis;

  /**
   * Root node of this controller's context. </p>
   *
   * Provides typed access not only to the elements of the root node 
   * but also to all nodes in the context (methods node<i>XYZ</i>()) 
   * and their currently selected element (methods current<i>XYZ</i>Element()). 
   * It also facilitates the creation of new elements for all nodes 
   * (methods create<i>XYZ</i>Element()). </p>
   *
   * @see com.manuli.wdp.IPrivateMDM_Fittings_form_CompView.IContextNode for more details.
   */
  private final IPrivateMDM_Fittings_form_CompView.IContextNode wdContext;

  /**
   * A shortcut for <code>wdThis.wdGetAPI()</code>. </p>
   * 
   * Represents the generic API of the generic Web Dynpro counterpart 
   * for this controller. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDViewController wdControllerAPI;
  
  /**
   * A shortcut for <code>wdThis.wdGetAPI().getComponent()</code>. </p>
   * 
   * Represents the generic API of the Web Dynpro component this controller 
   * belongs to. Can be used to access the message manager, the window manager,
   * to add/remove event handlers and so on. </p>
   */
  private final com.sap.tc.webdynpro.progmodel.api.IWDComponent wdComponentAPI;
  
  public MDM_Fittings_form_CompView(IPrivateMDM_Fittings_form_CompView wdThis)
  {
    this.wdThis = wdThis;
    this.wdContext = wdThis.wdGetContext();
    this.wdControllerAPI = wdThis.wdGetAPI();
    this.wdComponentAPI = wdThis.wdGetAPI().getComponent();
  }

  //@@begin javadoc:wdDoInit()
  /** Hook method called to initialize controller. */
  //@@end
  public void wdDoInit()
  {
    //@@begin wdDoInit()
    //@@end
  }

  //@@begin javadoc:wdDoExit()
  /** Hook method called to clean up controller. */
  //@@end
  public void wdDoExit()
  {
    //@@begin wdDoExit()
    //@@end
  }

  //@@begin javadoc:wdDoModifyView
  /**
   * Hook method called to modify a view just before rendering.
   * This method conceptually belongs to the view itself, not to the
   * controller (cf. MVC pattern).
   * It is made static to discourage a way of programming that
   * routinely stores references to UI elements in instance fields
   * for access by the view controller's event handlers, and so on.
   * The Web Dynpro programming model recommends that UI elements can
   * only be accessed by code executed within the call to this hook method.
   *
   * @param wdThis Generated private interface of the view's controller, as
   *        provided by Web Dynpro. Provides access to the view controller's
   *        outgoing controller usages, etc.
   * @param wdContext Generated interface of the view's context, as provided
   *        by Web Dynpro. Provides access to the view's data.
   * @param view The view's generic API, as provided by Web Dynpro.
   *        Provides access to UI elements.
   * @param firstTime Indicates whether the hook is called for the first time
   *        during the lifetime of the view.
   */
  //@@end
  public static void wdDoModifyView(IPrivateMDM_Fittings_form_CompView wdThis, IPrivateMDM_Fittings_form_CompView.IContextNode wdContext, com.sap.tc.webdynpro.progmodel.api.IWDView view, boolean firstTime)
  {
    //@@begin wdDoModifyView
    //@@end
  }

  //@@begin javadoc:onActionPrint1(ServerEvent)
  /** Declared validating event handler. */
  //@@end
  public void onActionPrint1(com.sap.tc.webdynpro.progmodel.api.IWDCustomEvent wdEvent )
  {
    //@@begin onActionPrint1(ServerEvent)
    try{

		String templateUrl = WDURLGenerator.getResourcePath(wdComponentAPI.getDeployableObjectPart(),"prova.xdp");
		InputStream template = new FileInputStream(templateUrl);

		String xmlData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
			"<ENTER_NAME_OF_ROOT_ELEMENT_HERE xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
			"<campo1>aaa</campo1>" +
			"<campo2>bbb</campo2>" +
			"</ENTER_NAME_OF_ROOT_ELEMENT_HERE>";

		ByteArrayInputStream dataSourceInputStream = new ByteArrayInputStream(xmlData.getBytes());
		ByteArrayOutputStream dataSourceOutputStream = new ByteArrayOutputStream();
		ByteArrayOutputStream templateSourceOutputStream = new ByteArrayOutputStream();

// convert InputStream to OutputStream
		IOUtil.write(template, templateSourceOutputStream);
		IOUtil.write(dataSourceInputStream,dataSourceOutputStream);
		IWDPDFDocumentCreationContext creationContext = WDPDFDocumentFactory.getDocumentHandler().getDocumentCreationContext();
		creationContext.setTemplate(templateSourceOutputStream);
		creationContext.setData(dataSourceOutputStream);

// PROVARE IL PASSAGGIO DI DATI DAL CONTESTO
// pdfContext.setData(WDInteractiveFormHelper.getContextDataAsStream(wdContext.nodeDataSource()));
		creationContext.setDynamic(true); //if you want to create dynamic interactive form
		IWDPDFDocument pdfDoc = creationContext.execute();
		
		String pdfString = new String(pdfDoc.getPDF());

		IWDWindow window = wdComponentAPI.getWindowManager().createNonModalExternalWindow(pdfString,"PDF Window");
		window.show();

//		pdfResource element is bounded to FileDownload UI element resource property
//		IWDWebResource pdfResource = WDWebResource.getWebResource(pdfDoc.getPDF(), WDWebResourceType.PDF); 
//	    wdContext.currentContextElement().setPdfResource(pdfResource); 
	  
    }catch( java.io.FileNotFoundException e ){
    	return;
    }catch( java.io.IOException e ){
    	return;
    }catch( WDPDFDocumentRenderException e ){
		return;
	}catch( Exception e ){
		return;
	};
	

    
    //@@end
  }

  /*
   * The following code section can be used for any Java code that is 
   * not to be visible to other controllers/views or that contains constructs
   * currently not supported directly by Web Dynpro (such as inner classes or
   * member variables etc.). </p>
   *
   * Note: The content of this section is in no way managed/controlled
   * by the Web Dynpro Designtime or the Web Dynpro Runtime. 
   */
  //@@begin others
  //@@end
}
